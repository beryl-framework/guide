# guide

The project used in the how-to-use guide of Beryl.

## Installation

```
clone https://gitlab.com/beryl-framework/guide
cd guide
shards install
crystal src/guide.cr
```

## Development

Will be improved at some point.

**The development of the framework is on standby for now. I have more pressing matter at the moment.**
