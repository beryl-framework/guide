require "beryl"
require "./guide/*"
require "./guide/models/*"
require "./guide/controllers/*"

module Guide
  get "/languages", with: LanguageResource, do: :index

  resource with: ProjectResource do |path|
    resource with: LanguageResource, path: path
  end
end

Beryl.config.content_type = "application/json"
Beryl.config.db = {
  "host" => "locahost",
  "port" => 5432,
  "name" => "berylGuide"
}
Beryl.run
