module Guide
  class LanguageResource < Beryl::Controller
    def index
      json(request.params["project_id"]? ? Language.where({ :project_id => request.params["project_id"] }) : Language.all)
    end

    def show
      json Language.find({ :id => request.params["language_id"], :project_id => request.params["project_id"] })
    end
  end
end
