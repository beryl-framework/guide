module Guide
  class ProjectResource < Beryl::Controller
    def index
      json Project.all
    end

    def show
      json Project.find(request.params["project_id"])
    end

    def create
      Project.create(request.data).save!
    end

    def update
      project = Project.find(request.params["project_id"])
      new_name = request.data["name"]
      if !project.nil?
        project.not_nil!.name = new_name.as(String)
        project.not_nil!.save!
      end
    end

    def destroy
      project = Project.find(request.params["project_id"])
      project.not_nil!.destroy! unless project.nil?
    end
  end
end
