module Guide
  class Language < Beryl::Model
    belongs_to Project
    
    property name : String
  end
end
