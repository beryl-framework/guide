module Guide
  class Project < Beryl::Model
    has_many Language

    property name : String?
    property url : String?
  end
end
